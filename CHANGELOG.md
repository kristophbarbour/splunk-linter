# Change Log

All notable changes to the "splunk-linter" extension will be documented in this file.

## [0.0.5] - 2019-07-11

Bug Fix, snippets

### Added

- Added serverClass snippets
    - serverclass-class = serverClass:<class>, serverclass-app = serverClass:<class>:app:<app_name>

### Changed

- Fixed issue with multiple variables in completion items causing duplicate placeholders

## [0.0.4] - 2019-07-04

`TIME_FORMAT` parsing (WIP), spec path setting and bug fixes

### Added

- Added parsing (mouse hover docs) for the `TIME_FORMAT` setting
    - Some variables aren't supported yet, for example: `%Ez`, `%::z`
    - Users can define their localization in the extension setting `splunk.timeLocalization`
- Spec files are now optionally loaded from a user defined setting `splunk.pathToSpecFiles`

### Changed

- Fixed issue with trying to load global/default stanza settings when there weren't any

## [0.0.3] - 2019-07-01

Bug Fixes and changelog

### Changed

- Fixed incorrect snippet param index for SHOULD_LINEMERGE
- Updated CHANGELOG.md to reflect previous changes

## [0.0.2] - 2019-07-01

Minor improvements

### Added

- Extension icon

### Changed

- Default Stanza's settings added to all Stanza's

## [0.0.1] - 2019-07-01

Initial release of splunk-linter extension with basic features

### Added

- Initial release
- Syntax Highlighting
- Code Completion
- Handy Snippets

## [Unreleased]

- Development release with code testing
