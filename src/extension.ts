import * as path from "path";
import * as vscode from "vscode";
import { ReadSpec, Conf, Stanza, Setting, GLOBAL_STANZA } from "./conf";
import { Strftime, Locale } from "./strftime";


const SPEC_PATH: string = "specs";
const TF: string = "TIME_FORMAT";
const TF_REX: RegExp = /TIME_FORMAT\s*=\s*/g;

let config: vscode.WorkspaceConfiguration | undefined;
let specPath: string;
let strftime: Strftime = new Strftime();

// First day of the year
const FDOY: Date = new Date(new Date().getFullYear(), 0, 1);
// Last day of the year, month is 0 based
const LDOY: Date = new Date(new Date().getFullYear(), 11, 31);

let loaded: any = {};
const getOpenFile = () => {
  return vscode.window.activeTextEditor
    ? path.basename(vscode.window.activeTextEditor.document.fileName)
    : "";
};

const buildCI = (text: string, docs: string, kind: vscode.CompletionItemKind, stanza: boolean = false) => {
  let ci = new vscode.CompletionItem(
    text,
    kind
  );
  ci.documentation = new vscode.MarkdownString(docs);
  // Convert to a snippet and extract variables/placeholders
  // Get `<value>`, allow for multiples
  let n = 0;
  let snipText = text.replace(/\<([^\>]+)\>/g, (m, p1) => {
    n++;
    return `$\{${n}:${p1}\}`;
  });
  ci.insertText = new vscode.SnippetString(stanza ? `${snipText}]` : snipText);

  return ci;
};

const BuildMDString = (lines: string[]): vscode.MarkdownString => {

  let md = new vscode.MarkdownString();
  for (let i = 0; i < lines.length; i++) {
    md.appendMarkdown(`${lines[i]}\n`);
  }

  return md;
};

const GenerateStanzaCompletionProvider = (context: vscode.ExtensionContext, conf: Conf) => {
  let provider = vscode.languages.registerCompletionItemProvider(
    { pattern: `**/${conf.name}`, language: "splunk", scheme: 'file' },
    {
      provideCompletionItems(
        document: vscode.TextDocument,
        position: vscode.Position
      ) {
        // get all text until the `position` and check if it reads `[`
        if (position.character > 1) {
          return undefined;
        }
        if (!document.lineAt(position.line).text.endsWith("[")) {
          return undefined;
        }

        let completionArr: vscode.CompletionItem[] = [];
        try {
          for (let i in conf.stanzas) {
            let stanza: Stanza = conf.stanzas[i];
            completionArr.push(buildCI(stanza.name, stanza.doc, vscode.CompletionItemKind.Class, true));
          }
        } catch (err) {
          console.log(err);
        }

        return completionArr;
      }
    },
    "["
  );

  context.subscriptions.push(provider);
};

const GenerateSettingCompletionProvider = (context: vscode.ExtensionContext, conf: Conf) => {
  let provider = vscode.languages.registerCompletionItemProvider(
    { pattern: `**/${conf.name}`, language: "splunk", scheme: 'file' },
    {
      provideCompletionItems(
        document: vscode.TextDocument,
        position: vscode.Position
      ) {

        if (position.character > 1) {
          return undefined;
        }

        // Get the nearest Stanza, if there is one
        let i = position.line;
        let curStanza = GLOBAL_STANZA;
        while (i >= 0) {
          if (document.lineAt(i).text[0] === "[") {
            curStanza = document.lineAt(i).text.replace(/[\[\]]/g, '');
            break;
          }
          i--;
        }


        let completionArr: vscode.CompletionItem[] = [];
        try {
          let settings: Setting[] = [];
          for (let i in conf.stanzas) {
            let stanza: Stanza = conf.stanzas[i];
            if (new RegExp(stanza.regex).test(curStanza)) {
              settings = stanza.settings;
            }
          }

          // Add all settings as CompletionItems
          for (let i = 0; i < settings.length; i++) {
            completionArr.push(buildCI(
              settings[i].name,
              settings[i].doc,
              vscode.CompletionItemKind.Property
            ));
          }

          // Add all global/default settings to every other stanza
          if (curStanza !== GLOBAL_STANZA && conf.stanzas.hasOwnProperty(GLOBAL_STANZA)) {
            settings = conf.stanzas[GLOBAL_STANZA].settings;
            for (let i = 0; i < settings.length; i++) {
              completionArr.push(buildCI(
                settings[i].name,
                settings[i].doc,
                vscode.CompletionItemKind.Property
              ));
            }
          }

        } catch (err) {
          console.log(err);
        }

        return completionArr;
      }
    }
  );

  context.subscriptions.push(provider);
};

const RegisterFileConfig = (context: vscode.ExtensionContext, e?: vscode.TextEditor | undefined) => {

  let openFile = getOpenFile();
  // Check if we've already loaded this conf file
  if (loaded.hasOwnProperty(openFile))
    return;
  ReadSpec(openFile, specPath)
    .then((conf: Conf) => {
      loaded[conf.name] = true;
      GenerateStanzaCompletionProvider(context, conf);
      GenerateSettingCompletionProvider(context, conf);

      const strftimeHover = vscode.languages.registerHoverProvider(
        { pattern: `**/${openFile}`, language: "splunk", scheme: 'file' },
        {
          provideHover(
            doc: vscode.TextDocument,
            pos: vscode.Position
          ) {
            let text = doc.lineAt(pos.line).text;
            if (text.startsWith(TF)) {
              let format = text.replace(TF_REX, '').trim();
              return new vscode.Hover(BuildMDString([
                'Expected Time Format',
                '',
                `* Now: ${strftime.parse(format, new Date())}`,
                `* First day of year: ${strftime.parse(format, FDOY)}`,
                `* Last day of year: ${strftime.parse(format, LDOY)}`
              ]));
            }
            return undefined;
          }
        }
      );

      context.subscriptions.push(strftimeHover);
    }, (err) => {
      console.log(err);
    });

};

export function activate(context: vscode.ExtensionContext) {
  // Load config
  config = vscode.workspace.getConfiguration();
  // Setup time localization
  let locale: any = config.get('splunk.timeLocalization');
  strftime = new Strftime(new Locale(locale.days, locale.shortDays, locale.months, locale.shortMonths, locale.AM, locale.am, locale.PM, locale.pm, locale.formats, locale.ordinalSuffixes));
  // Setup spec file location
  let configSpecPath = config.get('splunk.pathToSpecFiles');
  if (typeof configSpecPath === "undefined" || configSpecPath == null) {
    specPath = path.join(context.extensionPath, SPEC_PATH);
  } else if (typeof configSpecPath === "string") {
    specPath = configSpecPath;
  }

  // Load initial tab's spec file
  RegisterFileConfig(context);

  // Add listener for when the user switches file
  let stanzaConfig = vscode.window.onDidChangeActiveTextEditor(e => {
    RegisterFileConfig(context, e);
  });
}
