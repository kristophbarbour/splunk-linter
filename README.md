# splunk-linter README

Provides syntax highlighting for Splunk .conf files along with simple code completion and snippet generation.

## Features

### Syntax Highlighting

Syntax highlighting for Stanzas, settings and comments

![Highlighting](https://bitbucket.org/kristophbarbour/splunk-linter/raw/master/images/highlighting.jpg "Syntaxt Highlighting in action")

### Code Completion

Conf file aware Code Completion for Stanzas and settings. Simply activate by pressing Ctrl+Space/Cmd+Space

![Code Completion](https://bitbucket.org/kristophbarbour/splunk-linter/raw/master/images/codecompletion.gif "Using code completion")

### Handy Snippets

Handy Snippets for common Stanzas and settings

![Handy Snippets](https://bitbucket.org/kristophbarbour/splunk-linter/raw/master/images/snippet.gif "Using snippets")

## Requirements

None.

## Extension Settings

None.

## Known Issues

Some conf files parse incorrectly due to their respective spec files' lack of consistency.

## Release Notes

### 0.0.1

Initial release of splunk-linter extension with basic features:

- Syntax Highlighting
- Code Completion
- Handy Snippets

### 0.0.2

Minor improvement to Stanza generation and a new Icon :)

- Added default Stanza's setting to all Stanza's
- Added an icon

### 0.0.3

Bug fix and changelog.

- Fixed incorrect snippet param index for SHOULD_LINEMERGE
- Updated CHANGELOG.md

### 0.0.4

`TIME_FORMAT` parsing (WIP), spec path setting and bug fixes

- Added parsing (mouse hover docs) for the `TIME_FORMAT` setting
    - Some variables aren't supported yet, for example: `%Ez`, `%::z`
    - Users can define their localization in the extension setting `splunk.timeLocalization`
- Spec files are now optionally loaded from a user defined setting `splunk.pathToSpecFiles`
- Fixed issue with trying to load global/default stanza settings when there weren't any

### 0.0.5

Bug fix, snippets

- Fixed issue with multiple variables in completion items causing duplicate placeholders
- Added serverClass snippets
